module.exports = {
  extends: ['@commitlint/config-conventional'], 
  rules: {
    "header-max-length": [2, "always", 72],
    "body-leading-blank": [2, "always"],
    "body-max-line-length": [2, "always", 72],
    "type-enum": [0, "never", []],
    "type-empty": [0, "never"],
    "subject-empty": [2, "always"]
  }
};
