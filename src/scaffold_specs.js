#!/usr/bin/env node
'use strict'

const path = require('path')
const snakeCase = require('lodash.snakeCase')
const fs = require('fs-extra')

const {
  specActionsTemplate,
  specActionsEtagTemplate,
  specMutationsTemplate,
  specAppTemplate,
  specGettersTemplate,
} = require('./templates/index')

/**
 * scaffold the files inside the `assets` folder:
 *  - namespace
 *    - store
 *      | - actions_spec.js
 *      | - mutations_spec.js
 *      | - getters_spec.js
 *    - components
 *      |- app_spec.js
 * 
 * 
 * @param {Object} answers
 */

module.exports = (answers) => {
  const folderNamespace = snakeCase(answers.namespace)
  const namespace = snakeCase(answers.namespace)
  const specsPath = path.join('spec', 'javascripts', folderNamespace)
  const polling = answers.polling

  const actionsPath = path.join(specsPath, 'store', 'actions_spec.js')

  // store specs files
  fs.outputFile(path.join(specsPath, 'store', 'mutations_spec.js'), specMutationsTemplate(namespace, path.join(namespace, 'store')))
  fs.outputFile(path.join(specsPath, 'store', 'getters_spec.js'), specGettersTemplate(namespace, path.join(namespace, 'store')))
  
  if (polling === 'etag') {
    fs.outputFile(actionsPath, specActionsEtagTemplate(namespace, path.join(namespace, 'store')))
  } else {
    fs.outputFile(actionsPath, specActionsTemplate(namespace, path.join(namespace, 'store')))
  }

  // app spec file
  fs.outputFile(path.join(specsPath, 'components', 'app_spec.js'), specAppTemplate(namespace))
}
