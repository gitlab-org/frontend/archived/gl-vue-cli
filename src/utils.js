const fs = require('fs')
const chalk = require('chalk')
const path = require('path')

const error = chalk.bold.red
const success = chalk.bold.green

const hasAssestsNamespace = (namespace) => fs.existsSync(path.join('app' , 'assets', 'javascripts', namespace))
const hasSpecsNamesapce = (namespace) => fs.existsSync(path.join('spec', 'javascripts', namespace))

// eslint-disable-next-line
const logErrorMessage = (message) => console.error(error(message))
const createIssueError = (err) => logErrorMessage('There was an error while creating the app, please open an issue in https://gitlab.com/gitlab-org/gl-vue-cli/issues/new with the output of this error' + err)

module.exports = {
  error,
  success,
  logErrorMessage,
  hasAssestsNamespace,
  hasSpecsNamesapce,
  createIssueError
}