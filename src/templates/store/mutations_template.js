#!/usr/bin/env node
'use strict'

module.exports = (namespace) => {
  const upperCaseNamespace = namespace.toUpperCase()

  return `/**
  * Instructions:
  * - Every mutation should have jsdoc
 */
import * as types from './mutation_types';

export default {
  /**
  * Sets the main endpoint
  * @param {Object} state 
  * @param {String} endpoint 
  */
  [types.SET_ENDPOINT](state, endpoint){
    state.endpoint = endpoint;
  },

  /**
   * Sets isLoading to true while the request is being made.
   * @param {Object} state 
  */
  [types.REQUEST_${upperCaseNamespace}](state) {
    state.isLoading = true;
  },
  
  /**
   * Sets isLoading to false.
   * Sets hasError to false.
   * Sets the received data
   * @param {Object} state 
   * @param {Object} data 
  */
  [types.RECEIVE_${upperCaseNamespace}_SUCCESS](state, data) {
    state.hasError = false;
    state.isLoading = false;
    state.data = data;
  },

  /**
   * Sets isLoading to false.
   * Sets hasError to true.
   * Resets the data
   * @param {Object} state 
   * @param {Object} data 
  */
  [types.RECEIVE_${upperCaseNamespace}_ERROR](state) {
    state.isLoading = false;
    state.data = {};
    state.hasError = true;
  },
};
  `

}
