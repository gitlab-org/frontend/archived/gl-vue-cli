#!/usr/bin/env node
'use strict'

module.exports = () => `/**
* Instructions: replace the props with the ones that fit your app.
* These are guidelines for the suggested props that are common to most of our apps:
* 
* - endpoint {String}: 
* Used to store the main endpoint usually provided by haml. Allows easier access when used in the action to make requests.
* Some apps have more than 1 endpoint, we suggest using more than one prop or an object:
* // Option 1
* endpoints: {
*   get: {String},
* }
* // Option 2
* jobEndpoint: {String}
* traceEndpoint: {String}
* 
* - isLoading {Boolean}
* Used to indicate wether the request is being made or has already finished. Usually we render a loading spinner while this prop is true
* 
* - hasError {Boolean}
* Used to indicate wether the request returned an error.
* 
* - data {Object|Array}
* Used to store the received data in the main fetch request.
* 
* - pagination {Object}
* Used to store the pagination information when the endpoint is a paginated list.
*/
export default () => ({
  endpoint: null,
  isLoading: false,
  hasError: false,
  data: {}, 
  pagination: {},
});
`
