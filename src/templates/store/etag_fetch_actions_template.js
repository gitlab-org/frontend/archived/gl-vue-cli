#!/usr/bin/env node
'use strict'

const capitalize = require('lodash.capitalize')

module.exports = (namespace) => {
  const capitalizedNamesapce = capitalize(namespace)
  const upperCasedNamespace = namespace.toUpperCase()

  return `/**
* Instructions:
* - All actions should be documented with jsdoc
* - For actions that make a request to the API, the following pattern should be followed:
*  Instead of creating an action to toggle the loading state and dispatch it in the component, create: 
* 1. An action requestSomething, to toggle the loading state 
* 2. An action receiveSomethingSuccess, to handle the success callback 
* 3. An action receiveSomethingError, to handle the error callback 
* 4. An action fetchSomething to make the request. 
* 5. In case your application does more than a GET request you can use these as examples: 
*   1. PUT: createSomething 
*   2. POST: updateSomething 
*   3. DELETE: deleteSomething
* The component MUST only dispatch the fetchNamespace action. Actions namespaced with request or receive should not be called from the component The fetch action will be responsible to dispatch requestNamespace, receiveNamespaceSuccess and receiveNamespaceError
*/

import Visibility from 'visibilityjs';
import * as types from './mutation_types';
import axios from '~/lib/utils/axios_utils';
import Poll from '~/lib/utils/poll';
import createFlash from '~/flash';
import { __ } from '~/locale';

/**
 * Commits a mutation to store the main endpoint.
 * 
 * @param {String} endpoint 
 */
export const setEndpoint = ({ commit }, endpoint) => commit(types.SET_ENDPOINT, endpoint);
 
/**
 * Set up etag polling
 * 
 */
let eTagPoll;

export const clearEtagPoll = () => {
  eTagPoll = null;
};

export const stopPolling = () => {
  if (eTagPoll) eTagPoll.stop();
};

export const restartPolling = () => {
  if (eTagPoll) eTagPoll.restart();
};

/**
* Commits a mutation to update the state while the main endpoint is being requested.
*/
export const request${capitalizedNamesapce} = ({ commit }) => commit(types.${upperCasedNamespace});

/**
* Fetches the main endpoint.
* Will dispatch requestNamespace action before starting the request.
* Will dispatch receiveNamespaceSuccess if the request is successfull
* Will dispatch receiveNamesapceError if the request returns an error
*/
export const fetch${capitalizedNamesapce} = ({ state, dispatch }) => {
  dispatch('request${capitalizedNamesapce}');

  eTagPoll = new Poll({
    resource: {
      get${capitalizedNamesapce}(endpoint) {
        return axios.get(endpoint);
      },
    },
    data: state.endpoint,
    method: 'get${capitalizedNamesapce}',
    successCallback: ({ data }) => dispatch('receive${capitalizedNamesapce}Success', data),
    errorCallback: () => dispatch('receive${capitalizedNamesapce}Error'),
  });

  if (!Visibility.hidden()) {
    eTagPoll.makeRequest();
  } else {
    axios
      .get(state.endpoint)
      .then(({ data }) => dispatch('receive${capitalizedNamesapce}Success', data))
      .catch(() => dispatch('receive${capitalizedNamesapce}Error'))
  }

  Visibility.change(() => {
    if (!Visibility.hidden()) {
      dispatch('restartPolling');
    } else {
      dispatch('stopPolling');
    }
  });
};

export const receive${capitalizedNamesapce}Success = ({ commit }, data) => commit(types.RECEIVE_${upperCasedNamespace}_SUCCESS, data);

export const receive${capitalizedNamesapce}Error = ({commit}) => {
  commit(types.RECEIVE_${upperCasedNamespace}_ERROR);
  createFlash(__('An error occured while fetching ${namespace}. Please try again.'))
}

// prevent babel-plugin-rewire from generating an invalid default during karma tests
export default () => {};
`
}
