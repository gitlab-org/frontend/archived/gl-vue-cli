#!/usr/bin/env node
'use strict'

module.exports = (namespace, path) => {
  const upperCasedNamespace = namespace.toUpperCase()

  return `import * as getters from '~/${path}/getters';
import state from '~/${path}/state';

describe('${upperCasedNamespace} Store Getters', () => {
  let localState;

  beforeEach(() => {
    localState = state();
  });

  describe('description', () => {
    it('description', () => {
      localState.foo = 'foo';

      expect(getters.getter1(localState)).toEqual('foo')
    });
  });
  `
}