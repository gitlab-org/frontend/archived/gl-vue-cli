#!/usr/bin/env node
'use strict'

const capitalize = require('lodash.capitalize')

module.exports = (namespace, path) => {
  const capitalizedNamesapce = capitalize(namespace)
  const upperCasedNamespace = namespace.toUpperCase()

  return `import state from '~/${path}/state';
import mutations from '~/${path}/mutations';
import * as types from '~/${path}/mutation_types';

describe('${capitalizedNamesapce} Store Mutations', () => {
  let stateCopy;

  beforeEach(() => {
    stateCopy = state();
  });

  describe('SET_ENDPOINT', () => {
    it('should set endpoint', () => {
      mutations[types.SET_ENDPOINT](stateCopy, 'endpoint.json');

      expect(stateCopy.endpoint).toEqual('endpoint.json');
    });
  });

  describe('REQUEST_${upperCasedNamespace}', () => {
    it('sets isLoading to true', () => {
      mutations[types.REQUEST_${upperCasedNamespace}](stateCopy);

      expect(stateCopy.isLoading).toEqual(true);
    });
  });

  describe('RECEIVE_${upperCasedNamespace}_SUCCESS', () => {
    it('sets is loading to false', () => {
      mutations[types.RECEIVE_${upperCasedNamespace}_SUCCESS](stateCopy, { id: 1312321 });

      expect(stateCopy.isLoading).toEqual(false);
    });

    it('sets hasError to false', () => {
      mutations[types.RECEIVE_${upperCasedNamespace}_SUCCESS](stateCopy, { id: 1312321 });

      expect(stateCopy.hasError).toEqual(false);
    });

    it('sets data', () => {
      mutations[types.RECEIVE_${upperCasedNamespace}_SUCCESS](stateCopy, { id: 1312321 });

      expect(stateCopy.data).toEqual({ id: 1312321 });
    });
  });

  describe('RECEIVE_${upperCasedNamespace}_ERROR', () => {
    it('resets data', () => {
      mutations[types.RECEIVE_${upperCasedNamespace}_ERROR](stateCopy);

      expect(stateCopy.isLoading).toEqual(false);
      expect(stateCopy.data).toEqual({});
    });
  });
});
  `
}
